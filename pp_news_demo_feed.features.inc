<?php
/**
 * @file
 * pp_news_demo_feed.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_news_demo_feed_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
